<?php

namespace App\Controller;

use App\Repository\CausesRepository;
use App\Repository\EventRepository;
use App\Repository\NewsRepository;
use App\Repository\PostRepository;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * Affichage du bloc des causes
     * @param  PaginatorInterface $paginator        [description]
     * @param  CausesRepository   $causesRepository [description]
     * @param  Request            $request          [description]
     * @return [type]                               [description]
     */
    public function causes(PaginatorInterface $paginator, CausesRepository $causesRepository, Request $request):Response
    {
        $causes = $paginator->paginate(
            $causesRepository->getPublished(),
            $request->query->getInt('page', 1),
            9
        );

        return $this->render('home/sectionCauses.html.twig', compact('causes'));
    }


    public function shop(PaginatorInterface $paginator, ProductRepository $productRepository, Request $request)
    {
        $products = $paginator->paginate(
            $productRepository->getPublished(),
            $request->query->getInt('page', 1),
            9
        );

        return $this->render('home/sectionProducts.html.twig', compact('products'));
    }


    public function events(PaginatorInterface $paginator, EventRepository $eventRepository, Request $request)
    {
        $events = $paginator->paginate(
            $eventRepository->getPublished(),
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('home/sectionEvents.html.twig', compact('events'));
    }

    public function news(PaginatorInterface $paginator, NewsRepository $newsRepository, Request $request)
    {
        $news = $paginator->paginate(
            $newsRepository->getPublished(),
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('home/sectionNews.html.twig', compact('news'));
    }

    public function posts(PaginatorInterface $paginator, PostRepository $postRepository, Request $request)
    {
        $posts = $paginator->paginate(
            $postRepository->getPublished(),
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('home/sectionPost.html.twig', compact('posts'));
    }
}
