<?php

namespace App\Controller;

use App\Entity\CategoryProduct;
use App\Repository\CategoryProductRepository;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends AbstractController
{
    /**
     * @Route("/shop", name="shop.public")
     * @param PaginatorInterface $paginator
     * @param ProductRepository $productRepository
     * @param Request $request
     * @return Response
     */
    public function index(PaginatorInterface $paginator, CategoryProductRepository $categoryProductRepository, ProductRepository $productRepository, Request $request):Response
    {
        $products = $paginator->paginate(
            $productRepository->getPublished(),
            $request->query->getInt('page', 1),
            12
        );
        $categories = $categoryProductRepository->findAll();
        return $this->render('shop/index.html.twig', compact('products', 'categories'));
    }
}
