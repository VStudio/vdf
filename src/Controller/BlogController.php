<?php

namespace App\Controller;


use App\Entity\Post;
use App\Repository\CategoryPostRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog.public")
     * @param PaginatorInterface $paginator
     * @param CategoryPostRepository $categoryPostRepository
     * @param TagRepository $tagRepository
     * @param PostRepository $postRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginatorInterface $paginator,CategoryPostRepository $categoryPostRepository,TagRepository $tagRepository,PostRepository $postRepository, Request $request)
    {
        $posts = $paginator->paginate(
            $postRepository->getPostActive(),
            $request->query->getInt('page',1),12
        );
        $tags = $tagRepository->findAll();
        $categories = $categoryPostRepository->findAll();
        return $this->render('blog/index.html.twig', compact('categories','tags','posts'));
    }


    /**
     * @Route("/post/{slug}", name="post_details.public")
     */
    public function show(Post $post)
    {
        return $this->render('blog/details.html.twig', compact('post'));
    }
}
