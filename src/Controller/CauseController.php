<?php

namespace App\Controller;

use App\Entity\Causes;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CauseController
 * @package App\Controller
 */
class CauseController extends AbstractController
{
    /**
     * @Route("/cause", name="cause.public")
     * @param           PaginatorInterface $paginator
     * @param           Request            $request
     * @return          \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $causesRepository = $this->getDoctrine()->getRepository(Causes::class);
        $causes = $paginator->paginate(
            $causesRepository->getPublished(),
            $request->query->getInt('page', 1),
            12
        );
        return $this->render('cause/index.html.twig', compact('causes'));
    }

    /**
     * @Route("/cause/{slug}", name="cause_details.public")
     * @param Causes $cause
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Causes $cause)
    {
        return $this->render('cause/details.html.twig', compact('cause'));
    }
}
