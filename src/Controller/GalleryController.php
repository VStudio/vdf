<?php

namespace App\Controller;

use App\Repository\CategoryImageRepository;
use App\Entity\CategoryImage;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController extends AbstractController
{
    /**
     * @Route("/gallery", name="gallery.public")
     * @param CategoryImageRepository $categoryImageRepository
     * @param Request $request
     * @return Response
     */
    public function index(PaginatorInterface $paginator, CategoryImageRepository $categoryImageRepository, Request $request): Response
    {
        $categoryImages = $paginator->paginate(
            $categoryImageRepository->getAll(),
            $request->query->getInt('page', 1),
            12
        );
        return $this->render('gallery/index.html.twig', compact('categoryImages'));
    }

    /**
     * @Route("/gallery/{slug}", name="gallery_details.public")
     */
    public function show(CategoryImage $category)
    {
        return $this->render('gallery/details.html.twig', compact('category'));
    }
}
