<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DonateNowController extends AbstractController
{
    /**
     * @Route("/donation", name="donate_now.public")
     */
    public function index()
    {
        return $this->render('donate_now/index.html.twig');
    }
}
