<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PartialsController extends AbstractController
{

	/**
	 * @Route("/partials", name="partials")
	 */
	public function sponsors()
	{
		return $this->render('partials/sponsors.html.twig');
	}

	/**
	 * @Route("/partials", name="partials")
	 */
	public function testimonials()
	{
		return $this->render('partials/testimonials.html.twig');
	}
}
