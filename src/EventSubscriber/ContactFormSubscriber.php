<?php


namespace App\EventSubscriber;


use App\Entity\ContactMessage;
use App\Events;
use App\Services\EmailSender;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Mime\Email;

class ContactFormSubscriber implements EventSubscriberInterface
{

    /**
     * @var string
     */
    private $emailSender;
    /**
     * @var EmailSender
     */
    private $sender;

    public function __construct(EmailSender $sender, string $emailSender)
    {

        $this->emailSender = $emailSender;
        $this->sender = $sender;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            // le nom de l'event et le nom de la fonction qui sera déclenché
            Events::CONTACT_FORM_SUBMIT => 'onContactFormSubmit',
        ];
    }

    public function onContactFormSubmit(GenericEvent $event)
    {
        $entity = $event->getSubject();
        if ($entity instanceof ContactMessage) {
            $email = (new Email())->from($entity->getEmail())->to($this->emailSender)->subject
            ('Contact :'.$entity->getSubject())
                ->html('<p>'.$entity->getMessage().'</p>');
            $this->sender->sendNotification($email);
        }

    }
}