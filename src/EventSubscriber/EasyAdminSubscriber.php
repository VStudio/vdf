<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(TokenStorageInterface $tokenStorage, RouterInterface $router)
    {
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    /**
     * @param $event
     */
    public function onEasyAdminPrePersist($event)
    {
        $entity = $event->getSubject();

        if (($this->tokenStorage->getToken() instanceof User)
            && \property_exists($entity, 'userCreator')
            && \method_exists($entity, 'setUserCreator')
        ) {

            $entity->setUserCreator($this->tokenStorage->getToken()->getUser());

        }
    }

    public static function getSubscribedEvents():array
    {
        return [
            'easy_admin.pre_persist' => 'onEasyAdminPrePersist',
        ];
    }
}
