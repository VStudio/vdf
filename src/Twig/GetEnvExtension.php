<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class GetEnvExtension extends AbstractExtension
{

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getEnv', [$this, 'getEnv'])
        ];
    }

    public function getEnv(string $env)
    {
        return getenv($env);
    }
}
