<?php

namespace App\Twig;

use Ramsey\Uuid\UuidInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Doctrine\UuidEncoderDoctrine;

class UuidExtension extends AbstractExtension
{
    /**
     * @var UuidEncoderDoctrine
     */
    private $encoder;

    public function __construct(UuidEncoderDoctrine $encoder)
    {
        $this->encoder = $encoder;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('uuid_encode', [$this, 'doEncodeUuid'], ['is_safe'=>['html']]),
        ];
    }

    public function doEncodeUuid(UuidInterface $uuid):string
    {
        return $this->encoder->encode($uuid);
    }
}
