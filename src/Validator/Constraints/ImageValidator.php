<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ImageValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {
        if($protocol->getImgFile() === null && $protocol->getImg() === null && $protocol->getUpdatedAt() === null)
        {
            return $this->context
                ->buildViolation($constraint->message)
                ->atPath('imgFile')
                ->addViolation()
                ;
        }
    }
}
