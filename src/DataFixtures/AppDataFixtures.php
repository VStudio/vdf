<?php

namespace App\DataFixtures;

use App\Entity\CategoryImage;
use App\Entity\ContactMessage;
use App\Entity\CategoryPost;
use App\Entity\Causes;
use App\Entity\Currency;
use App\Entity\Event;
use App\Entity\News;
use App\Entity\Picture;
use App\Entity\Post;
use App\Entity\Tag;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppDataFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var \Faker\Generator
     */
    private $faker;
    /**
     * @var string
     */
    private $fakePicture;

    /**
     * @var string
     */
    private $content;

    /**
     * AppDataFixtures constructor.
     *
     * @param UserPasswordEncoderInterface $encoder
     * @param string $fakePicture
     */
    public function __construct(UserPasswordEncoderInterface $encoder, string $fakePicture)
    {
        $this->encoder = $encoder;
        $this->faker = Factory::create('fr_FR');
        $this->fakePicture = $fakePicture;
    }

    public function load(ObjectManager $manager)
    {
        $boolean = [true, false];
        $roles = [
            0 => ['Administrator', 'ROLE_ADMIN'],
            1 => ['User', 'ROLE_USER']
        ];
        $user = new User();
        $hash = $this->encoder->encodePassword($user, 'password2019');
        $user->setEmail('ongvoixdesfemmes@gmail.com')
            ->setNom('ONG')
            ->setPrenom('VoixDesFemmes')
            ->setRoles([$roles[0][1]])
            ->setPassword($hash);
        $manager->persist($user);

        //Ajout des devises
        $devises = [
            1 => ['libelle' => 'Euro', 'code' => 'EUR'],
            2 => ['libelle' => 'Dollar US', 'code' => 'USD'],
            3 => ['libelle' => 'Dollar Canada', 'code' => 'CAD']
        ];

        foreach ($devises as $devise) {
            $currency = new Currency();
            $currency->setCode($devise['code'])->setTitle($devise['libelle'])->setUserCreator($user);
            $manager->persist($currency);
        }
//
//        //Ajout des Categories des articles
        $libelleCategoriesArticles = ['Technology', 'Lifestyle', 'Fashion', 'Art', 'Food', 'Architecture', 'Adventure'];
        $categoryArray = [];
        foreach ($libelleCategoriesArticles as $categoriesArticle) {
            $categoryImage = new CategoryImage();
            $categoryImage->setTitle($categoriesArticle)->setUserCreator($user);
            $manager->persist($categoryImage);
            $categoryPost = new CategoryPost();
            $categoryPost->setTitle($categoriesArticle)->setUserCreator($user);
            $manager->persist($categoryPost);
            $categoryArray[] = $categoryPost;
        }
//
//        //Ajout des Tags
        $tagsArray = [];
        foreach ($libelleCategoriesArticles as $tags) {
            $tag = new Tag();
            $tag->setName($tags)->setUserCreator($user);
            $manager->persist($tag);
            $tagsArray[] = $tag;
        }
//
//        //Generation des images
//        $postImages = $this->generatePictures('blog', 10);
//        $this->generatePictures('news', 10);
//        $this->generatePictures('event', 10);
//        $this->generatePictures('causes', 10);
//
//        //Ajout des articles pour chaque category
        for ($i = 0; $i < 100; ++$i) {
            $post = new Post();
            $post->setTitle($this->faker->words(4, true))
               ->setUserCreator($user)
               //->setImgFile($this->faker->randomElement($postImages))
               ->setCategory($this->faker->randomElement($categoryArray))
               ->setContent($this->getContent())
               ->setTags($this->faker->randomElements($tagsArray))
               ->setIsPublished($this->faker->randomElement($boolean));
            $manager->persist($post);
        }
//

//        //Ajout des causes
        for ($i = 0; $i < 100; ++$i) {
            $cause = new Causes();
            $cause->setTitle($this->faker->words(4, true))
               ->setUserCreator($user)
               //->setImgFile($this->faker->randomElement($galleryImages))
               ->setContent($this->getContent())
               ->setIsPublished($this->faker->randomElement($boolean))
               ->setTotalNeeded($this->faker->numberBetween(500, 20000))
               ->setStartDate($this->faker->dateTimeBetween('now', '+ 45 days'))
               ->setEndDate($this->faker->dateTimeBetween('now', '+ 45 days'))
               ->setIsActive($this->faker->randomElement($boolean));
            $manager->persist($cause);
        }
//
//        //Ajout des evenements
        for ($i = 0; $i < 100; ++$i) {
            $event = new Event();
            $event->setTitle($this->faker->words(4, true))
               ->setUserCreator($user)
               //->setImgFile($this->faker->randomElement($galleryImages))
               ->setContent($this->getContent())
               ->setAddress($this->faker->address)
               ->setCity($this->faker->city)
               ->setIsPublished($this->faker->randomElement($boolean))
               ->setStartDate($this->faker->dateTimeBetween('now', '+ 45 days'))
               ->setNumberOfPlace($this->faker->numberBetween(10, 200))
               ->setEndDate($this->faker->dateTimeBetween('now', '+ 45 days'))
               ->setIsActive($this->faker->randomElement($boolean));
            $manager->persist($event);
        }

        //Ajout des news
        for ($i = 0; $i < 100; ++$i) {
            $news = new News();
            $news->setTitle($this->faker->words(4, true))
               ->setUserCreator($user)
               //->setImgFile($this->faker->randomElement($galleryImages))
               ->setContent($this->getContent())
               ->setIsPublished($this->faker->randomElement($boolean));
            $manager->persist($news);
        }

        //Ajout des messages contact
        for ($i=0; $i < 500; $i++) {
            $message = new ContactMessage();
            $message->setName($this->faker->name)->setEmail($this->faker->email)->setPhone($this->faker->phoneNumber)->setSubject($this->faker->word)->setMessage($this->faker->paragraph(4, true))->setIpAddress($this->faker->ipv4);
            $manager->persist($message);
            //            die(dump($message));
        }
        $manager->flush();
    }

    /**
     * @param string $directory
     * @param int $nbr
     * @return array
     */
    private function generatePictures(string $directory, int $nbr = 5): array
    {
        $dir = $this->fakePicture . $directory;
        $finder = new Finder();
        $filesystem = new Filesystem();
        if (!$filesystem->exists($dir)) {
            $filesystem->mkdir($dir, 0700);
        }
        $finder->files()->in($dir);
        while ($finder->hasResults() === false) {
            //generation d'images aleatoires
            for ($i = 0; $i < $nbr; ++$i) {
                $this->faker->image($dir, 1142, 664);
            }
        }

        foreach ($finder as $item) {
            $data[] = new UploadedFile($item->getRealPath(), $item->getFilename());
        }

        return $data;
    }


    private function getContent()
    {
        return <<<CONTENUE
        <h1>Fortemne possumus dicere eundem illum Torquatum?</h1>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Sed plane dicit quod intellegit.</b> Quo modo autem optimum, si bonum praeterea nullum est? In eo autem voluptas omnium Latine loquentium more ponitur, cum percipitur ea, quae sensum aliquem moveat, iucunditas. Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant? <a href="http://loripsum.net/" target="_blank">Prave, nequiter, turpiter cenabat;</a> Duo Reges: constructio interrete. <a href="http://loripsum.net/" target="_blank">Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae.</a> Ne in odium veniam, si amicum destitero tueri. Iam illud quale tandem est, bona praeterita non effluere sapienti, mala meminisse non oportere? </p>

<p>Ita enim se Athenis collocavit, ut sit paene unus ex Atticis, ut id etiam cognomen videatur habiturus. Se dicere inter honestum et turpe nimium quantum, nescio quid inmensum, inter ceteras res nihil omnino interesse. Critolaus imitari voluit antiquos, et quidem est gravitate proximus, et redundat oratio, ac tamen is quidem in patriis institutis manet. Est igitur officium eius generis, quod nec in bonis ponatur nec in contrariis. Praeterea et appetendi et refugiendi et omnino rerum gerendarum initia proficiscuntur aut a voluptate aut a dolore. <i>Torquatus, is qui consul cum Cn.</i> </p>

<dl>
	<dt><dfn>Ut pulsi recurrant?</dfn></dt>
	<dd>Pauca mutat vel plura sane;</dd>
	<dt><dfn>Quid vero?</dfn></dt>
	<dd>Quid enim de amicitia statueris utilitatis causa expetenda vides.</dd>
	<dt><dfn>Haeret in salebra.</dfn></dt>
	<dd>Cuius quidem, quoniam Stoicus fuit, sententia condemnata mihi videtur esse inanitas ista verborum.</dd>
	<dt><dfn>Avaritiamne minuis?</dfn></dt>
	<dd>Est igitur officium eius generis, quod nec in bonis ponatur nec in contrariis.</dd>
</dl>
<ul>
	<li>Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate.</li>
	<li>Quae cum magnifice primo dici viderentur, considerata minus probabantur.</li>
	<li>Si qua in iis corrigere voluit, deteriora fecit.</li>
	<li>Huius, Lyco, oratione locuples, rebus ipsis ielunior.</li>
</ul>
<p><i>At ille pellit, qui permulcet sensum voluptate.</i> <b>Summus dolor plures dies manere non potest?</b> Non est enim vitium in oratione solum, sed etiam in moribus. Quos qui tollunt et nihil posse percipi dicunt, ii remotis sensibus ne id ipsum quidem expedire possunt, quod disserunt. Graecis hoc modicum est: Leonidas, Epaminondas, tres aliqui aut quattuor; <b>Hoc non est positum in nostra actione.</b> Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster? Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim? <i>Quid ait Aristoteles reliquique Platonis alumni?</i> Nec vero umquam summum bonum assequi quisquam posset, si omnia illa, quae sunt extra, quamquam expetenda, summo bono continerentur. <mark>Ad eos igitur converte te, quaeso.</mark> </p>
<ol>
	<li>Etenim semper illud extra est, quod arte comprehenditur.</li>
	<li>Esse enim quam vellet iniquus iustus poterat inpune.</li>
	<li>Nunc omni virtuti vitium contrario nomine opponitur.</li>
	<li>Paulum, cum regem Persem captum adduceret, eodem flumine invectio?</li>
</ol>
<p>Minime vero probatur huic disciplinae, de qua loquor, aut iustitiam aut amicitiam propter utilitates adscisci aut probari. <b>Duo enim genera quae erant, fecit tria.</b> <i>Minime vero, inquit ille, consentit.</i> Non dolere, inquam, istud quam vim habeat postea videro; Possumusne hic scire qualis sit, nisi contulerimus inter nos, cum finem bonorum dixerimus, quid finis, quid etiam sit ipsum bonum? Deprehensus omnem poenam contemnet. Quid, cum fictas fabulas, e quibus utilitas nulla elici potest, cum voluptate legimus? Quis est tam dissimile homini. Igitur neque stultorum quisquam beatus neque sapientium non beatus. Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. </p>
<blockquote cite="http://loripsum.net">
	Atqui pugnantibus et contrariis studiis consiliisque semper utens nihil quieti videre, nihil tranquilli potest.
</blockquote>
CONTENUE;
    }
}
