<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use App\Entity\Event;
use App\Entity\User;
use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var Generator
     */
    private $faker;


    /**
     * AppDataFixtures constructor.
     *
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->faker = Factory::create();
    }

    /**
     * @param ObjectManager $manager
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        $boolean = [true, false];
        $user = new User();
        $hash = $this->encoder->encodePassword($user, 'password2019');
        $user->setEmail('ongvoixdesfemmes@gmail.com')
            ->setNom('ONG')
            ->setPrenom('VoixDesFemmes')
            ->setPassword($hash);
        $manager->persist($user);

        //Ajout des devises
        $devises = [
            1 => ['libelle' => 'Euro', 'code' => 'EUR'],
            2 => ['libelle' => 'Dollar US', 'code' => 'USD'],
            3 => ['libelle' => 'Dollar Canada', 'code' => 'CAD']
        ];

        foreach ($devises as $devise) {
            $currency = new Currency();
            $currency->setCode($devise['code'])->setTitle($devise['libelle'])->setUserCreator($user);
            $manager->persist($currency);
        }

        for ($i = 0; $i <= 10; $i++) {
            $event = new Event();
            $event->setIsPublished($this->faker->randomElement([true, false]))
                ->setCity($this->faker->city)
                ->setAddress($this->faker->address)
                ->setContent($this->content())
                ->setUserCreator($user)
                ->setStartDate(Carbon::now()->addDay(1))
                ->setNumberOfPlace($this->faker->numberBetween(10, 200))
                ->setTitle($this->faker->title);
            $manager->persist($event);

        }

        try {
            $manager->flush();
        } catch (Exception $exception) {
            dd($exception);
        }

    }

    public function content()
    {
        return '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>';

    }
}
