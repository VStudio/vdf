<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NatureRepository")
 */
class Nature extends Donation
{
	/**
	 * @ORM\Column(type="text")
	 */
	protected $address;

	/**
	 * @ORM\Column(type="text")
	 * @Assert\NotBlank()
	 */
	protected $content;

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
