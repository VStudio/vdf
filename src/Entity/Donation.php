<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DonationRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn( name = "donate_type", type = "string" )
 * @ORM\DiscriminatorMap({
 *     "mobileMonney" = "MobileMonney",
 *     "bankTransfert" = "BankTransfert",
 *     "nature" = "Nature",
 *     "creditCard" = "CreditCard",
 *     "paypal" = "Paypal"
 *     })
 */
abstract class Donation
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Causes", inversedBy="donations")
	 */
	protected $cause;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\Email()
	 * @Assert\NotBlank()
	 */
	protected $email;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 */
	protected $phone;


	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $dateCreated;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $ipAddress;

	/**
	 * ContactMessage constructor.
	 * @throws \Exception
	 */
	public function __construct()
      	{
      		$this->dateCreated = new \DateTime();
      	}

	public function getId(): ?int
      	{
      		return $this->id;
      	}

	public function getCause(): ?Causes
      	{
      		return $this->cause;
      	}

	public function setCause(?Causes $cause): self
      	{
      		$this->cause = $cause;
      
      		return $this;
      	}

	public function getName(): ?string
      	{
      		return $this->name;
      	}

	public function setName(string $name): self
      	{
      		$this->name = $name;
      
      		return $this;
      	}

	public function getEmail(): ?string
      	{
      		return $this->email;
      	}

	public function setEmail(string $email): self
      	{
      		$this->email = $email;
      
      		return $this;
      	}

	public function getPhone(): ?string
      	{
      		return $this->phone;
      	}

	public function setPhone(string $phone): self
      	{
      		$this->phone = $phone;
      
      		return $this;
      	}

	public function getDateCreated(): ?\DateTimeInterface
      	{
      		return $this->dateCreated;
      	}

	public function setDateCreated(\DateTimeInterface $dateCreated): self
      	{
      		$this->dateCreated = $dateCreated;
      
      		return $this;
      	}

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(?string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }
}
