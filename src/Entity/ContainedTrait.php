<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Image
 */
trait ContainedTrait
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;
    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Slug(fields={"title"})
     * @Assert\Regex(pattern="/[a-z0-9\-]+$/")
     */
    protected $slug;

    /**
     * @ORM\Column(type="text",nullable=true)
     * @Assert\Length(max=160)
     */
    protected $extrait;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    protected $content;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Picture",cascade={"remove","persist"})
     */
    protected $pictures;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $is_published;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $publicationDate;

    public function __construct()
    {
        $this->is_published = false;
        parent::__construct();
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersistContained()
    {
        if ($this->is_published === true) {
            $this->publicationDate = new DateTime();
        }
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getExtrait(): ?string
    {
        return $this->extrait;
    }

    public function setExtrait(string $extrait): self
    {
        $this->extrait = $extrait;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->is_published;
    }

    public function setIsPublished(bool $is_published): self
    {
        $this->is_published = $is_published;

        return $this;
    }

    public function getPublicationDate(): ?DateTimeInterface
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(?DateTimeInterface $publicationDate): self
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $Picture): self
    {
        if (!$this->pictures->contains($Picture)) {
            $this->pictures[] = $Picture;
            $Picture->setEvent($this);
        }

        return $this;
    }

    public function removePicture(Picture $Picture): self
    {
        if ($this->pictures->contains($Picture)) {
            $this->pictures->removeElement($Picture);
            // set the owning side to null (unless already changed)
            if ($Picture->getEvent() === $this) {
                $Picture->setEvent(null);
            }
        }

        return $this;
    }
}
