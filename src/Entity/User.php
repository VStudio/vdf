<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $prenom;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;
    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tag", mappedBy="userCreator", orphanRemoval=true)
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CategoryPost", mappedBy="userCreator", orphanRemoval=true)
     */
    private $categoryPosts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sponsor", mappedBy="userCreator", orphanRemoval=true)
     */
    private $sponsors;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\News", mappedBy="userCreator", orphanRemoval=true)
     */
    private $news;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="userCreator")
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Event", mappedBy="userCreator", orphanRemoval=true)
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Causes", mappedBy="userCreator", orphanRemoval=true)
     */
    private $causes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="userCreator", orphanRemoval=true)
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Currency", mappedBy="userCreator", orphanRemoval=true)
     */
    private $currencies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CategoryImage", mappedBy="userCreator", orphanRemoval=true)
     */
    private $categoryImages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CategoryProduct", mappedBy="userCreator", orphanRemoval=true)
     */
    private $categoryProducts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TypeOfCreditCard", mappedBy="userCreator", orphanRemoval=true)
     */
    private $typeOfCreditCards;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Picture", mappedBy="userCreator", orphanRemoval=true)
     */
    private $pictures;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->categoryPosts = new ArrayCollection();
        $this->categoryProducts = new ArrayCollection();
        $this->sponsors = new ArrayCollection();
        $this->news = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->causes = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->currencies = new ArrayCollection();
        $this->categoryImages = new ArrayCollection();
        $this->typeOfCreditCards = new ArrayCollection();
        $this->pictures = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return $this->email;
    }

    public function getFullName(): ?string
    {
        return $this->nom.' '.$this->prenom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }


    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * Returns the roles or permissions granted to the user for security.
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        // guarantees that a user always has at least one role for security
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->setUserCreator($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            // set the owning side to null (unless already changed)
            if ($tag->getUserCreator() === $this) {
                $tag->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CategoryPost[]
     */
    public function getCategoryPosts(): Collection
    {
        return $this->categoryPosts;
    }

    public function addCategoryPost(CategoryPost $categoryPost): self
    {
        if (!$this->categoryPosts->contains($categoryPost)) {
            $this->categoryPosts[] = $categoryPost;
            $categoryPost->setUserCreator($this);
        }

        return $this;
    }

    public function removeCategoryPost(CategoryPost $categoryPost): self
    {
        if ($this->categoryPosts->contains($categoryPost)) {
            $this->categoryPosts->removeElement($categoryPost);
            // set the owning side to null (unless already changed)
            if ($categoryPost->getUserCreator() === $this) {
                $categoryPost->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Sponsor[]
     */
    public function getSponsors(): Collection
    {
        return $this->sponsors;
    }

    public function addSponsor(Sponsor $sponsor): self
    {
        if (!$this->sponsors->contains($sponsor)) {
            $this->sponsors[] = $sponsor;
            $sponsor->setUserCreator($this);
        }

        return $this;
    }

    public function removeSponsor(Sponsor $sponsor): self
    {
        if ($this->sponsors->contains($sponsor)) {
            $this->sponsors->removeElement($sponsor);
            // set the owning side to null (unless already changed)
            if ($sponsor->getUserCreator() === $this) {
                $sponsor->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|News[]
     */
    public function getNews(): Collection
    {
        return $this->news;
    }

    public function addNews(News $news): self
    {
        if (!$this->news->contains($news)) {
            $this->news[] = $news;
            $news->setUserCreator($this);
        }

        return $this;
    }

    public function removeNews(News $news): self
    {
        if ($this->news->contains($news)) {
            $this->news->removeElement($news);
            // set the owning side to null (unless already changed)
            if ($news->getUserCreator() === $this) {
                $news->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setUserCreator($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getUserCreator() === $this) {
                $post->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setUserCreator($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getUserCreator() === $this) {
                $event->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Causes[]
     */
    public function getCauses(): Collection
    {
        return $this->causes;
    }

    public function addCause(Causes $cause): self
    {
        if (!$this->causes->contains($cause)) {
            $this->causes[] = $cause;
            $cause->setUserCreator($this);
        }

        return $this;
    }

    public function removeCause(Causes $cause): self
    {
        if ($this->causes->contains($cause)) {
            $this->causes->removeElement($cause);
            // set the owning side to null (unless already changed)
            if ($cause->getUserCreator() === $this) {
                $cause->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Currency[]
     */
    public function getCurrencies(): Collection
    {
        return $this->currencies;
    }

    public function addCurrency(Currency $currency): self
    {
        if (!$this->currencies->contains($currency)) {
            $this->currencies[] = $currency;
            $currency->setUserCreator($this);
        }

        return $this;
    }

    public function removeCurrency(Currency $currency): self
    {
        if ($this->currencies->contains($currency)) {
            $this->currencies->removeElement($currency);
            // set the owning side to null (unless already changed)
            if ($currency->getUserCreator() === $this) {
                $currency->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CategoryImage[]
     */
    public function getCategoryImages(): Collection
    {
        return $this->categoryImages;
    }

    public function addCategoryImage(CategoryImage $categoryImage): self
    {
        if (!$this->categoryImages->contains($categoryImage)) {
            $this->categoryImages[] = $categoryImage;
            $categoryImage->setUserCreator($this);
        }

        return $this;
    }

    public function removeCategoryImage(CategoryImage $categoryImage): self
    {
        if ($this->categoryImages->contains($categoryImage)) {
            $this->categoryImages->removeElement($categoryImage);
            // set the owning side to null (unless already changed)
            if ($categoryImage->getUserCreator() === $this) {
                $categoryImage->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TypeOfCreditCard[]
     */
    public function getTypeOfCreditCards(): Collection
    {
        return $this->typeOfCreditCards;
    }

    public function addTypeOfCreditCard(TypeOfCreditCard $typeOfCreditCard): self
    {
        if (!$this->typeOfCreditCards->contains($typeOfCreditCard)) {
            $this->typeOfCreditCards[] = $typeOfCreditCard;
            $typeOfCreditCard->setUserCreator($this);
        }

        return $this;
    }

    public function removeTypeOfCreditCard(TypeOfCreditCard $typeOfCreditCard): self
    {
        if ($this->typeOfCreditCards->contains($typeOfCreditCard)) {
            $this->typeOfCreditCards->removeElement($typeOfCreditCard);
            // set the owning side to null (unless already changed)
            if ($typeOfCreditCard->getUserCreator() === $this) {
                $typeOfCreditCard->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setUserCreator($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->contains($picture)) {
            $this->pictures->removeElement($picture);
            // set the owning side to null (unless already changed)
            if ($picture->getUserCreator() === $this) {
                $picture->setUserCreator(null);
            }
        }

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setUserCreator($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getUserCreator() === $this) {
                $product->setUserCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CategoryProduct[]
     */
    public function getCategoryProducts(): Collection
    {
        return $this->categoryProducts;
    }

    public function addCategoryProduct(CategoryProduct $categoryProduct): self
    {
        if (!$this->categoryProducts->contains($categoryProduct)) {
            $this->categoryProducts[] = $categoryProduct;
            $categoryProduct->setUserCreator($this);
        }

        return $this;
    }

    public function removeCategoryProduct(CategoryProduct $categoryProduct): self
    {
        if ($this->categoryProducts->contains($categoryProduct)) {
            $this->categoryProducts->removeElement($categoryProduct);
            // set the owning side to null (unless already changed)
            if ($categoryProduct->getUserCreator() === $this) {
                $categoryProduct->setUserCreator(null);
            }
        }

        return $this;
    }
}
