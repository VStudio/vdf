<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BankTransfertRepository")
 */
class BankTransfert extends Donation
{
	/**
	 * @ORM\Column(type="string")
	 */
	protected $bankAccount;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency", inversedBy="bankTransferts")
     */
    protected $currency;

    public function getBankAccount(): ?string
    {
        return $this->bankAccount;
    }

    public function setBankAccount(string $bankAccount): self
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
}
