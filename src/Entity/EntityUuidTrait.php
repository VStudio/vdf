<?php
namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\HasLifecycleCallbacks()
 */
trait EntityUuidTrait
{

    /**
     * The internal primary identity key.
     *
     * @var UuidInterface|null
     *
     * @ORM\Column(type="uuid", unique=true)
     */
    protected $uuid;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateCreated;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $dateUpdate;

    /**
     * @ORM\PrePersist()
     */
    public function prePersistEntity()
    {
        $this->setUuid();
        $this->dateCreated = new DateTime();
    }
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdateEntity()
    {
        $this->dateUpdate = new DateTime();
    }

    public function setUuid()
    {
        $this->uuid = Uuid::uuid4();
        return $this;
    }
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
    /**
     * @return mixed
     */
    public function getImgFile(): ?File
    {
        return $this->imgFile;
    }

    /**
     * @param File|null $image
     * @return Causes
     * @throws \Exception
     */
    public function setImgFile(File $image = null): self
    {
        $this->imgFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateUpdate = new DateTime('now');
        }
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateCreated(): DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param DateTime $dateCreated
     * @return EntityUuidTrait
     */
    public function setDateCreated(DateTime $dateCreated): EntityUuidTrait
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * @param mixed $dateUpdate
     * @return EntityUuidTrait
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
        return $this;
    }
}
