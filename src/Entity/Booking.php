<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $email;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $phone;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $price;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $numberOfPlace;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $comment;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Currency", inversedBy="bookings")
	 */
	private $currency;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $dateCreated;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Event", inversedBy="bookings")
	 */
	private $event;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $ipAddress;

	public function __construct()
      	{
      		$this->dateCreated = new \DateTime();
      	}

	public function __toString()
      	{
      		return 'Name: '.$this->name . ' - Place(s): ' .$this->numberOfPlace;
      	}

	public function getId(): ?int
      	{
      		return $this->id;
      	}

	public function getName(): ?string
      	{
      		return $this->name;
      	}

	public function setName(string $name): self
      	{
      		$this->name = $name;
      
      		return $this;
      	}

	public function getEmail(): ?string
      	{
      		return $this->email;
      	}

	public function setEmail(string $email): self
      	{
      		$this->email = $email;
      
      		return $this;
      	}

	public function getPhone(): ?string
      	{
      		return $this->phone;
      	}

	public function setPhone(string $phone): self
      	{
      		$this->phone = $phone;
      
      		return $this;
      	}

	public function getPrice(): ?int
      	{
      		return $this->price;
      	}

	public function setPrice(?int $price): self
      	{
      		$this->price = $price;
      
      		return $this;
      	}

	public function getNumberOfPlace(): ?int
      	{
      		return $this->numberOfPlace;
      	}

	public function setNumberOfPlace(int $numberOfPlace): self
      	{
      		$this->numberOfPlace = $numberOfPlace;
      
      		return $this;
      	}

	public function getComment(): ?string
      	{
      		return $this->comment;
      	}

	public function setComment(?string $comment): self
      	{
      		$this->comment = $comment;
      
      		return $this;
      	}

	public function getCurrency(): ?Currency
      	{
      		return $this->currency;
      	}

	public function setCurrency(?Currency $currency): self
      	{
      		$this->currency = $currency;
      
      		return $this;
      	}

	public function getDateCreated(): ?\DateTimeInterface
      	{
      		return $this->dateCreated;
      	}

	public function setDateCreated(\DateTimeInterface $dateCreated): self
      	{
      		$this->dateCreated = $dateCreated;
      
      		return $this;
      	}

	public function getEvent(): ?Event
      	{
      		return $this->event;
      	}

	public function setEvent(?Event $event): self
      	{
      		$this->event = $event;
      
      		return $this;
      	}

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(?string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }
}
