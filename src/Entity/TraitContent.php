<?php


namespace App\Entity;


trait TraitContent
{

    public function getExtrait()
    {
        return \mb_substr($this->content, 0, 50);
    }
}