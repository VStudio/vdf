<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaypalRepository")
 */
class Paypal extends Donation
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency", inversedBy="paypals")
     */
    protected $currency;

	/**
	 * @ORM\Column(type="float")
     * @Assert\PositiveOrZero()
	 */
	protected $amount;

	/**
	 * @ORM\Column(type="string", length=255,nullable=true)
	 *
	 */
	protected $payement_id;

	/**
	 * @ORM\Column(type="string", length=255,nullable=true)
	 *
	 */
	protected $transaction_id;

	/**
	 * @ORM\Column(type="string", length=255,nullable=true)
	 *
	 */
	protected $payer_id;

	/**
	 * @ORM\Column(type="string", length=255,nullable=true)
	 *
	 */
	protected $payer_name;

	/**
	 * @ORM\Column(type="boolean")
	 *
	 */
	protected $status;

	/**
	 * @ORM\Column(type="string", length=255,nullable=true)
	 *
	 */
	protected $payer_email;

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPayementId(): ?string
    {
        return $this->payement_id;
    }

    public function setPayementId(?string $payement_id): self
    {
        $this->payement_id = $payement_id;

        return $this;
    }

    public function getTransactionId(): ?string
    {
        return $this->transaction_id;
    }

    public function setTransactionId(?string $transaction_id): self
    {
        $this->transaction_id = $transaction_id;

        return $this;
    }

    public function getPayerId(): ?string
    {
        return $this->payer_id;
    }

    public function setPayerId(?string $payer_id): self
    {
        $this->payer_id = $payer_id;

        return $this;
    }

    public function getPayerName(): ?string
    {
        return $this->payer_name;
    }

    public function setPayerName(?string $payer_name): self
    {
        $this->payer_name = $payer_name;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPayerEmail(): ?string
    {
        return $this->payer_email;
    }

    public function setPayerEmail(?string $payer_email): self
    {
        $this->payer_email = $payer_email;

        return $this;
    }
}
