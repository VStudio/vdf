<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MobileMonneyRepository")
 */
class MobileMonney extends Donation
{
	/**
	 * @ORM\Column(type="float")
     * @Assert\PositiveOrZero()
	 */
	protected $amount;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $idTransfert;

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getIdTransfert(): ?string
    {
        return $this->idTransfert;
    }

    public function setIdTransfert(string $idTransfert): self
    {
        $this->idTransfert = $idTransfert;

        return $this;
    }

}
