<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CausesRepository")
 */
class Causes
{
    use ContainedTrait;
    use EntityUuidTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;
    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $startDate;
    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $endDate;
    /**
     * @ORM\Column(type="float")
     * @Assert\PositiveOrZero()
     */
    private $totalNeeded;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Donation", mappedBy="cause")
     */
    private $donations;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="causes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userCreator;

    public function __construct()
    {
        $this->donations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getTotalNeeded(): ?float
    {
        return $this->totalNeeded;
    }

    public function setTotalNeeded(float $totalNeeded): self
    {
        $this->totalNeeded = $totalNeeded;

        return $this;
    }

    /**
     * @return Collection|Donation[]
     */
    public function getDonations(): Collection
    {
        return $this->donations;
    }

    public function addDonation(Donation $donation): self
    {
        if (!$this->donations->contains($donation)) {
            $this->donations[] = $donation;
            $donation->setCause($this);
        }

        return $this;
    }

    public function removeDonation(Donation $donation): self
    {
        if ($this->donations->contains($donation)) {
            $this->donations->removeElement($donation);
            // set the owning side to null (unless already changed)
            if ($donation->getCause() === $this) {
                $donation->setCause(null);
            }
        }

        return $this;
    }

    public function getUserCreator(): ?User
    {
        return $this->userCreator;
    }

    public function setUserCreator(?User $userCreator): self
    {
        $this->userCreator = $userCreator;

        return $this;
    }

}
