<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @Vich\Uploadable
 */
class Product
{
    use ContainedTrait;
    use EntityUuidTrait;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoryProduct", inversedBy="products")
     */
    protected $category;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    protected $price;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $img;

    /**
     * @var File
     * @Vich\UploadableField(mapping="product", fileNameProperty="img")
     * @Assert\Image()
     *
     * @var File|null
     */
    protected $imgFile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $userCreator;

    public function __construct()
    {
        $this->price = 0;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?CategoryProduct
    {
        return $this->category;
    }

    public function setCategory(?CategoryProduct $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @param File|null $image
     * @return Post
     * @throws \Exception
     */
    public function setImgFile(File $image = null): self
    {
        $this->imgFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateUpdate = new \DateTime('now');
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImgFile(): ?File
    {
        return $this->imgFile;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getUserCreator(): ?User
    {
        return $this->userCreator;
    }

    public function setUserCreator(?User $userCreator): self
    {
        $this->userCreator = $userCreator;

        return $this;
    }
}
