<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRepository")
 */
class Currency
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $code;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="currency")
     */
    private $bookings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BankTransfert", mappedBy="currency")
     */
    private $bankTransferts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Paypal", mappedBy="currency")
     */
    private $paypals;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CreditCard", mappedBy="currency")
     */
    private $creditCards;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="currencies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userCreator;

    /**
     * Currency constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->dateCreated = new DateTime();
        $this->bookings = new ArrayCollection();
        $this->bankTransferts = new ArrayCollection();
        $this->paypals = new ArrayCollection();
        $this->creditCards = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setCurrency($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getCurrency() === $this) {
                $booking->setCurrency(null);
            }
        }

        return $this;
    }

    public function getDateCreated(): ?DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return Collection|BankTransfert[]
     */
    public function getBankTransferts(): Collection
    {
        return $this->bankTransferts;
    }

    public function addBankTransfert(BankTransfert $bankTransfert): self
    {
        if (!$this->bankTransferts->contains($bankTransfert)) {
            $this->bankTransferts[] = $bankTransfert;
            $bankTransfert->setCurrency($this);
        }

        return $this;
    }

    public function removeBankTransfert(BankTransfert $bankTransfert): self
    {
        if ($this->bankTransferts->contains($bankTransfert)) {
            $this->bankTransferts->removeElement($bankTransfert);
            // set the owning side to null (unless already changed)
            if ($bankTransfert->getCurrency() === $this) {
                $bankTransfert->setCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Paypal[]
     */
    public function getPaypals(): Collection
    {
        return $this->paypals;
    }

    public function addPaypal(Paypal $paypal): self
    {
        if (!$this->paypals->contains($paypal)) {
            $this->paypals[] = $paypal;
            $paypal->setCurrency($this);
        }

        return $this;
    }

    public function removePaypal(Paypal $paypal): self
    {
        if ($this->paypals->contains($paypal)) {
            $this->paypals->removeElement($paypal);
            // set the owning side to null (unless already changed)
            if ($paypal->getCurrency() === $this) {
                $paypal->setCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CreditCard[]
     */
    public function getCreditCards(): Collection
    {
        return $this->creditCards;
    }

    public function addCreditCard(CreditCard $creditCard): self
    {
        if (!$this->creditCards->contains($creditCard)) {
            $this->creditCards[] = $creditCard;
            $creditCard->setCurrency($this);
        }

        return $this;
    }

    public function removeCreditCard(CreditCard $creditCard): self
    {
        if ($this->creditCards->contains($creditCard)) {
            $this->creditCards->removeElement($creditCard);
            // set the owning side to null (unless already changed)
            if ($creditCard->getCurrency() === $this) {
                $creditCard->setCurrency(null);
            }
        }

        return $this;
    }

    public function getUserCreator(): ?User
    {
        return $this->userCreator;
    }

    public function setUserCreator(?User $userCreator): self
    {
        $this->userCreator = $userCreator;

        return $this;
    }
}
