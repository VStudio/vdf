<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CreditCardRepository")
 */
class CreditCard extends Donation
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency", inversedBy="creditCards")
     */
    protected $currency;

    /**
     * @ORM\Column(type="float")
     * @Assert\PositiveOrZero()
     */
    protected $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeOfCreditCard", inversedBy="creditCards")
     */
    private $typeOfCreditCard;

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getTypeOfCreditCard(): ?TypeOfCreditCard
    {
        return $this->typeOfCreditCard;
    }

    public function setTypeOfCreditCard(?TypeOfCreditCard $typeOfCreditCard): self
    {
        $this->typeOfCreditCard = $typeOfCreditCard;

        return $this;
    }
}
