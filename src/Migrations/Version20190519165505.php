<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190519165505 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sponsor CHANGE date_update date_update DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE donation CHANGE cause_id cause_id INT DEFAULT NULL, CHANGE ip_address ip_address VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE news CHANGE date_update date_update DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_transfert CHANGE currency_id currency_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE booking CHANGE currency_id currency_id INT DEFAULT NULL, CHANGE event_id event_id INT DEFAULT NULL, CHANGE price price INT DEFAULT NULL, CHANGE ip_address ip_address VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE credit_card CHANGE currency_id currency_id INT DEFAULT NULL, CHANGE type_of_credit_card_id type_of_credit_card_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE event CHANGE date_update date_update DATETIME DEFAULT NULL, CHANGE start_date start_date DATETIME DEFAULT NULL, CHANGE end_date end_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE picture CHANGE category_id category_id INT DEFAULT NULL, CHANGE date_update date_update DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE contact_message CHANGE ip_address ip_address VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE paypal CHANGE currency_id currency_id INT DEFAULT NULL, CHANGE payement_id payement_id VARCHAR(255) DEFAULT NULL, CHANGE transaction_id transaction_id VARCHAR(255) DEFAULT NULL, CHANGE payer_id payer_id VARCHAR(255) DEFAULT NULL, CHANGE payer_name payer_name VARCHAR(255) DEFAULT NULL, CHANGE payer_email payer_email VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE category_image CHANGE date_update date_update DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE causes CHANGE date_update date_update DATETIME DEFAULT NULL, CHANGE start_date start_date DATETIME DEFAULT NULL, CHANGE end_date end_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE post CHANGE category_id category_id INT DEFAULT NULL, CHANGE date_update date_update DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bank_transfert CHANGE currency_id currency_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE booking CHANGE currency_id currency_id INT DEFAULT NULL, CHANGE event_id event_id INT DEFAULT NULL, CHANGE price price INT DEFAULT NULL, CHANGE ip_address ip_address VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE category_image CHANGE date_update date_update DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE causes CHANGE date_update date_update DATETIME DEFAULT \'NULL\', CHANGE start_date start_date DATETIME DEFAULT \'NULL\', CHANGE end_date end_date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE contact_message CHANGE ip_address ip_address VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE credit_card CHANGE currency_id currency_id INT DEFAULT NULL, CHANGE type_of_credit_card_id type_of_credit_card_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE donation CHANGE cause_id cause_id INT DEFAULT NULL, CHANGE ip_address ip_address VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE event CHANGE date_update date_update DATETIME DEFAULT \'NULL\', CHANGE start_date start_date DATETIME DEFAULT \'NULL\', CHANGE end_date end_date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE news CHANGE date_update date_update DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE paypal CHANGE currency_id currency_id INT DEFAULT NULL, CHANGE payement_id payement_id VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE transaction_id transaction_id VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE payer_id payer_id VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE payer_name payer_name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE payer_email payer_email VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE picture CHANGE category_id category_id INT DEFAULT NULL, CHANGE date_update date_update DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE post CHANGE category_id category_id INT DEFAULT NULL, CHANGE date_update date_update DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE sponsor CHANGE date_update date_update DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
