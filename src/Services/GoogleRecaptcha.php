<?php


namespace App\Services;


use ReCaptcha\ReCaptcha;

class GoogleRecaptcha
{
    /**
     * @var string
     */
    private $secret;
    /**
     * @var string
     */
    private $key;

    public function __construct(string $secret, string $key)
    {

        $this->secret = $secret;
        $this->key = $key;
    }

    public function verify(string $gRecaptchaResponse, $remoteIp = null)
    {
        
        $recaptcha = new  ReCaptcha($this->secret);

        return $recaptcha->verify($gRecaptchaResponse, $remoteIp);
//            return $recaptcha->setExpectedHostname($domaine)->verify($gRecaptchaResponse, $remoteIp);


    }
}
