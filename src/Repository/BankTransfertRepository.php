<?php

namespace App\Repository;

use App\Entity\BankTransfert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BankTransfert|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankTransfert|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankTransfert[]    findAll()
 * @method BankTransfert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankTransfertRepository extends DonationRepository
{

}
