<?php

namespace App\Repository;

use App\Entity\Contained;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Contained|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contained|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contained[]    findAll()
 * @method Contained[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContainedRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Contained::class);
    }

    public function findAllActive()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.is_active= :val')
            ->andWhere('c.is_published= :val')
            ->andwhere('c INSTANCE OF :get_called_class')
            ->setParameter('val', true)
            ->setParameter('get_called_class', get_called_class())
            ->orderBy('c.publicationDate', 'DESC')
            ->getQuery();
    }


    // /**
    //  * @return Contained[] Returns an array of Contained objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contained
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
