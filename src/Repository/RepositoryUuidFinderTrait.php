<?php

namespace App\Repository;

use App\Doctrine\UuidEncoderDoctrine;

trait RepositoryUuidFinderTrait
{
    /**
     * @var UuidEncoderDoctrine
     */
    protected $uuidEncoder;

    public function findOneByEncodedUuid(string $encodedUuid)
    {
        return $this->findOneBy([
            'uuid' => $this->uuidEncoder->decode($encodedUuid)
        ]);
    }
}
