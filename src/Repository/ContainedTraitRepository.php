<?php

namespace App\Repository;


Trait ContainedTraitRepository
{

    public function getPublished()
    {
        $query = $this->createQueryBuilder('e')
            ->andWhere('e.is_published = :val')
            ->setParameter('val', true);

        if (method_exists($this->getEntityName(), 'getStartDate')) {
            $query->orderBy('e.startDate', 'ASC');
        }
        if (method_exists($this->getEntityName(), 'getPublicationDate')) {
            $query->orderBy('e.publicationDate', 'ASC');
        }
        return $query->getQuery();
    }


    // /**
    //  * @return Contained[] Returns an array of Contained objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contained
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
