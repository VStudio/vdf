<?php

namespace App\Repository;

use App\Entity\TypeOfCreditCard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypeOfCreditCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeOfCreditCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeOfCreditCard[]    findAll()
 * @method TypeOfCreditCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeOfCreditCardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypeOfCreditCard::class);
    }

    // /**
    //  * @return TypeOfCreditCard[] Returns an array of TypeOfCreditCard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeOfCreditCard
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
