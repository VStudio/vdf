<?php

namespace App\Repository;

use App\Entity\MobileMonney;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MobileMonney|null find($id, $lockMode = null, $lockVersion = null)
 * @method MobileMonney|null findOneBy(array $criteria, array $orderBy = null)
 * @method MobileMonney[]    findAll()
 * @method MobileMonney[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MobileMonneyRepository extends DonationRepository
{

}
